import {PluginFunction} from "@/codegen/types";

export const generateImports: PluginFunction = (
    schema,
    documents,
    config
) => {
    const defaultImports = [
        `import {QueryInputArg, QueryResponse, query, mutate, createGraphQlClient, ClientOptions, MutationInputArg, MutationResponse, QueryInputArgDocumentOptional, GeneratedQueryResponse, GeneratedMutationResponse} from 'buro26-graphql-codegen-client'`,
        `import {Mutation, Query} from "${config.typesImportPath}";`
    ]

    const imports: string[] = []

    return [...imports, ...defaultImports].join('\n')
}