import {Types} from '@graphql-codegen/plugin-helpers';
import {GraphQLField} from "graphql/type/definition";
import {generateImports} from "@/codegen/imports";
import {PluginFunction} from "@/codegen/types";

export const plugin: PluginFunction = (
    schema,
    documents,
    config
) => {
    const typesToImport: string[] = []
    const schemasToImport: string[] = []

    const generateMethodsFromSchema = (fields: GraphQLField<any, any>[], type: 'Query' | 'Mutation') => {
        return fields
            .map(field => {
                const name = field.name
                const nameUpper = `${name.substring(0, 1).toUpperCase()}${name.substring(1, name.length)}`
                const typeAction = type === 'Query' ? 'query' : 'mutate'

                schemasToImport.push(`${nameUpper}${type}InputSchema`)

                if (field.args.length === 0) {
                    return `    ${name}: {
                schema: ${nameUpper}${type}InputSchema,
                fetch: (options: ${type}InputArg<${type}['${name}']>): Promise<${type}Response<${type}['${name}']>> => ${typeAction}(graphQlClient, options),
            },`;
                }

                typesToImport.push(`${type}${nameUpper}Args`)

                return `    ${name}: {
                schema: ${nameUpper}${type}InputSchema,
                fetch: (options: ${type}InputArg<${type}['${name}'], ${type}${nameUpper}Args>): Promise<${type}Response<${type}['${name}']>> => ${typeAction}(graphQlClient, options),
            },`;
            })
            .filter(field => field !== null)
            .join('\n        ');
    }

    const generateMethodsFromDocument = (fields: Types.DocumentFile[], type: 'Query' | 'Mutation') => {
        const typeAction = type === 'Query' ? 'query' : 'mutate'

        return fields
            .flatMap(document => document.document?.definitions)
            .filter(def => def?.kind === 'OperationDefinition' && def?.operation === type.toLowerCase())
            .map(document => {
                // @ts-ignore
                const name = document.name.value
                const nameUpper = `${name.substring(0, 1).toUpperCase()}${name.substring(1, name.length)}`

                typesToImport.push(`${nameUpper}${type}Variables`)
                typesToImport.push(`${nameUpper}${type}`)
                typesToImport.push(`${nameUpper}Document`)
                schemasToImport.push(`${nameUpper}SchemaGenerated`)

                return `    ${name}: {
                    schema: ${nameUpper}SchemaGenerated, // @ts-ignore
                    fetch: (options: ${type}InputArgDocumentOptional<${type}['${name}'], ${nameUpper}${type}Variables>): Promise<Generated${type}Response<${nameUpper}${type}>> => ${typeAction}<any>(
                        graphQlClient,
                        {
                            ...options,
                            ${type.toLowerCase()}: options?.${type.toLowerCase()} || ${nameUpper}Document,
                            ${type === 'Mutation' ? ' update: () => {},' : ''}
                        }
                    )${type === 'Mutation' ? ` as Promise<Generated${type}Response<${nameUpper}${type}>>` : ''},
                },`;
            })
            .filter(field => field !== null)
            .filter((value, index, self) => self.indexOf(value) === index)
            .join('\n            ');
    }

    const createClientFunc = `
export const createClient = (options: ClientOptions) => {
    const graphQlClient = createGraphQlClient(options)
    
    return {
        query: {
        ${generateMethodsFromSchema(Object.values(schema.getQueryType()?.getFields() || {}), 'Query')}
            generated: {
            ${generateMethodsFromDocument(documents, 'Query')}
            },
        },
        mutate: {
        ${generateMethodsFromSchema(Object.values(schema.getMutationType()?.getFields() || {}), 'Mutation')}
            generated: {
            ${generateMethodsFromDocument(documents, 'Mutation')}
            },
        },
    }
}
    `;

    return `${generateImports(schema, documents, config)}
import {
   ${typesToImport.join(',\n   ')}
} from '${config.typesImportPath}'
import {
   ${schemasToImport.join(',\n   ')}
} from '${config.schemaImportPath}'


${createClientFunc}
        `
}
