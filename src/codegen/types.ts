import {PluginFunction as CodegenPluginFunction} from '@graphql-codegen/plugin-helpers';

export type Config = {
    logger: boolean
    typesImportPath: string
    schemaImportPath: string
}

export type PluginFunction = {} & CodegenPluginFunction<Config>