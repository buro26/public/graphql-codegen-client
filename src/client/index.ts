import {
    AnyVariables,
    Client,
    CombinedError,
    fetchExchange,
    mapExchange,
    Operation,
    OperationContext,
    OperationResult,
    TypedDocumentNode,
} from '@urql/core'
import * as GraphQLWeb from '@0no-co/graphql.web'
import * as GraphQL from 'graphql'
import jsonStringify from 'json-stable-stringify'
import {logError} from "@/client/logging";

type OrNever<T> = 0 extends 1 & T ? never : T
type DocumentNode = GraphQLWeb.DocumentNode | OrNever<GraphQL.DocumentNode>

export type ClientOptions = {
    url: string
    logging?: boolean
}

export const createGraphQlClient = (options: ClientOptions) => {
    const exchanges = []

    if (options?.logging) {
        exchanges.push(mapExchange({
            onError(error: CombinedError, operation: Operation) {
                logError(error, operation)
            }
        }))
    }

    return new Client({
        url: options.url,
        exchanges: [
            ...exchanges,
            fetchExchange,
        ],
        requestPolicy: 'network-only'
    })
}

const parseOptions = <T extends OperationOptions<any>>(options: T): T => {

    return {
        ...options,
        throwOnError: options?.throwOnError ?? true,
        throwOnNoToken: options?.throwOnNoToken ?? false,
        onError: options?.onError ?? (() => {
        })
    }
}

const extractToken = <T extends OperationOptions<any>>(options: T) => {
    if (options?.authToken) {
        return options.authToken
    }

    if (options?.throwOnNoToken) {
        throw new Error('No token found')
    }

    return null
}

export type OperationOptions<TVariables extends AnyVariables = AnyVariables> = {
    variables?: TVariables
    throwOnError?: boolean
    onError?: (error: CombinedError) => void
    authToken?: string
    throwOnNoToken?: boolean
    context?: OperationContext
    headers?: Record<string, string>
}

export type QueryOptions<T, TVariables extends AnyVariables = AnyVariables> = {
    query: TypedDocumentNode<T, TVariables>
} & OperationOptions<TVariables>

export type QueryInputArg<T, TVariables extends AnyVariables = AnyVariables> = QueryOptions<T, TVariables>
export type QueryInputArgDocumentOptional<T, TVariables extends AnyVariables = AnyVariables> =
    Omit<QueryOptions<T, TVariables>, 'query'>
    & { query?: DocumentNode }

export type QueryResponse<T> = OperationResult<{
    [key: string]: NonNullable<T>
}>
export type GeneratedQueryResponse<T> = OperationResult<T>

const requestMap = new Map<string, { request: Promise<OperationResult<any>>, timestamp: number }>()
const MIN_CACHE_DURATION = 500

export const dedupe = async <T, TVariables extends AnyVariables = AnyVariables>(
    options: QueryInputArg<T, TVariables>,
    queryFn: () => Promise<OperationResult<T>>
): Promise<OperationResult<T>> => {
    const token = extractToken(options)

    const queryId = jsonStringify({
        query: GraphQL.print(options.query),
        variables: options.variables,
        token,
    })

    const now = Date.now()

    if (requestMap.has(queryId)) {
        const cachedRequest = requestMap.get(queryId)!

        if (now - cachedRequest.timestamp < MIN_CACHE_DURATION || cachedRequest.request) {
            return cachedRequest.request
        }
    }

    const request = queryFn()
    requestMap.set(queryId, {request, timestamp: now})

    try {
        const result = await request

        // Ensure the request remains in the cache for at least MIN_CACHE_DURATION
        const elapsed = Date.now() - now

        if (elapsed < MIN_CACHE_DURATION) {
            await new Promise(resolve => setTimeout(resolve, MIN_CACHE_DURATION - elapsed))
        }

        requestMap.delete(queryId)

        return result
    } catch (error) {
        requestMap.delete(queryId)

        throw error
    }
}

export const query = async <T, TVariables extends AnyVariables = AnyVariables>(
    client: Client,
    options: QueryInputArg<T, TVariables>
): Promise<QueryResponse<T>> => {
    options = parseOptions(options)

    const token = extractToken(options)

    if (options?.throwOnNoToken && !token) {
        throw new Error('No token found')
    }

    const result = await dedupe(options, () =>
        client.query(
            options.query,
            options?.variables!,
            {
                ...(options?.context || {}),
                fetch: async (uri, fetchOptions) => {
                    return fetch(uri, {
                        ...fetchOptions,
                        headers: {
                            ...(fetchOptions?.headers ?? {}),
                            ...(options?.headers ?? {}),
                            authorization: token
                                ? `Bearer ${token}`
                                : ''
                        }
                    })
                }
            }
        ).toPromise()
    )

    if (result?.error) {
        options?.onError?.(result.error)

        if (options.throwOnError) {
            throw result.error
        }
    }

    // @ts-ignore
    return result
}

export type MutationOptions<T, TVariables extends AnyVariables = AnyVariables> = {
    mutation: TypedDocumentNode<T, TVariables>
} & OperationOptions<TVariables>

export type MutationInputArg<T, TVariables extends AnyVariables = AnyVariables> = MutationOptions<T, TVariables>
export type MutationInputArgDocumentOptional<T, TVariables extends AnyVariables = AnyVariables> =
    Omit<MutationOptions<T, TVariables>, 'mutation'>
    & { mutation?: DocumentNode | string }

export type MutationResponse<T> = OperationResult<{
    [key: string]: NonNullable<T>
}>

export type GeneratedMutationResponse<T> = OperationResult<T>

export const mutate = async <T, TVariables extends AnyVariables = AnyVariables>(
    client: Client,
    options: MutationInputArg<T, TVariables>
): Promise<MutationResponse<T>> => {
    options = parseOptions(options)

    const token = extractToken(options)

    const result = await client.mutation(
        options?.mutation,
        options?.variables!,
        {
            fetch: async (uri, fetchOptions) => {
                return fetch(uri, {
                    ...fetchOptions,
                    headers: {
                        ...(fetchOptions?.headers ?? {}),
                        ...(options?.headers ?? {}),
                        authorization: token
                            ? `Bearer ${token}`
                            : ''
                    }
                })
            }
        }
    ).toPromise()

    if (result?.error) {
        options?.onError?.(result.error)

        if (options.throwOnError) {
            throw result.error
        }
    }

    // @ts-ignore
    return result
}