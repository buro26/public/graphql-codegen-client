import chalk from 'chalk'
import { CombinedError, Operation } from '@urql/core'

export const logError = ({ graphQLErrors, networkError }: CombinedError, operation: Operation) => {
    if (graphQLErrors) {
        graphQLErrors.forEach(({ message, locations, path, extensions }) => {
            console.log(chalk.red(`error - ${path?.join(', ')}`))
            console.log(chalk.blue('Message:'), message)
            console.log(chalk.blue(`Location:`), locations)
            console.log(chalk.blue(`Type:`), extensions?.code) // @ts-ignore
            console.log(chalk.blue(`Stacktrace:`), extensions?.exception?.stacktrace)
        })
    }

    if (networkError) {
        console.log(chalk.red(`error - ${networkError.name}`))

        // @ts-ignore
        networkError?.result?.errors?.forEach((error: any) => {
            console.log(chalk.blue('Message:'), error.message)
            console.log(chalk.blue(`Location:`), error.locations)
            console.log(chalk.blue(`Type:`), error.extensions?.code) // @ts-ignore
            console.log(chalk.blue(`Stacktrace:`), error.extensions?.exception?.stacktrace)
        })
    }

}